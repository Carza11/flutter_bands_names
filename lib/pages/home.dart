import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';

import 'package:band_names/models/band.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Band> bands = [
    Band(id: '1', name: 'Queen', votes: 5),
    Band(id: '2', name: 'Guns and Roses', votes: 6),
    Band(id: '3', name: 'Metallica', votes: 7),
    Band(id: '4', name: 'Aereosmit', votes: 8),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(
              child: Text(
            'Bandas',
            textAlign: TextAlign.center,
          )),
          backgroundColor: Colors.orange,
        ),
        body: ListView.builder(
          itemCount: bands.length,
          itemBuilder: (context, i) => _bandTile(bands[i]),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            addNewBand();
          },
          child: Icon(Icons.add),
          elevation: 1,
        ));
  }

  Widget _bandTile(Band bandas) {
    return Dismissible(
      key: Key(bandas.id),
      direction: DismissDirection.startToEnd,
      onDismissed: (direction) {
        print(direction);
        //llamar el borrado en el sercer
      },
      background: Container(
        padding: EdgeInsets.only(left: 8.0),
        color: Colors.red,
        child: Align(
            child: Text(
          'Borrar Banda',
          style: TextStyle(color: Colors.white),
        )),
      ),
      child: ListTile(
        leading: CircleAvatar(
          child: Text(bandas.name.substring(0, 2)),
          backgroundColor: Colors.blue[100],
        ),
        title: Text(bandas.name),
        trailing: Text(
          '${bandas.votes}',
        ),
        onTap: () {
          print(bandas.name);
        },
      ),
    );
  }

  addNewBand() {
    final textController = new TextEditingController();
    //asegurarse de imortar del dart:io y no del dart:html
    if (Platform.isAndroid) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: Text('Nueva Banda'),
                content: TextField(
                  controller: textController,
                ),
                actions: [
                  MaterialButton(
                    onPressed: () {
                      addBandToist(textController.text);
                    },
                    child: Text('Add'),
                    elevation: 5,
                    textColor: Colors.blue,
                  )
                ],
              ));
    } else if (Platform.isIOS) {
      showCupertinoDialog(
          context: context,
          builder: (_) {
            return CupertinoAlertDialog(
              title: Text('New band name:'),
              content: CupertinoTextField(
                controller: textController,
              ),
              actions: [
                CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text('add'),
                  onPressed: () => addBandToist(textController.text),
                ),
                CupertinoDialogAction(
                  isDestructiveAction: true,
                  isDefaultAction: true,
                  child: Text('Dismiss'),
                  onPressed: () => Navigator.pop(context),
                )
              ],
            );
          });
    }
  }

  void addBandToist(String name) {
    if (name.length > 1) {
      //podemos agregar
      this
          .bands
          .add(new Band(id: DateTime.now().toString(), name: name, votes: 0));
      setState(() {});
    }
    Navigator.pop(context);
  }
}
